import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default.component';
import { RouterModule } from '@angular/router';



import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentPrincipalTestModule } from 'src/app/modules/component-principal-test/component-principal-test.module';



@NgModule({
  imports: [CommonModule, RouterModule, SharedModule, ComponentPrincipalTestModule],
  declarations: [
    DefaultComponent
  ],
  entryComponents: []
})
export class DefaultModule {}
