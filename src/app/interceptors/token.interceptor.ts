import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';


import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  browser: any;
  versionBrowser: any;
  osname :any;

  constructor() {
    this.browser=this.myBrowser();
    this.versionBrowser=this.getBrowserVersion();
    this.osname= this.versionOS();
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const verificateUrl = request.url.includes('assets');
    

  
    const headers = {
      
      'Content-Type': 'application/json',
      "x-user-browser" : this.versionBrowser,
      "x-user-os" : 'osx',
      
    };

    const reqClone = request.clone({
      setHeaders: headers
    });

    return next.handle(reqClone);
  }

  myBrowser() { 
    if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) {
        return 'Opera';
    }else if(navigator.userAgent.indexOf("Chrome") != -1 ){
        return 'Chrome';
    }else if(navigator.userAgent.indexOf("Safari") != -1){
        return 'Safari';
    }else if(navigator.userAgent.indexOf("Firefox") != -1 ) {
         return 'Firefox';
    }else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document == true )){
      return 'IE'; 
    } else {
       return 'unknown';
    }
}
 
getBrowserVersion(){
    var userAgent= navigator.userAgent, tem, 
    matchTest= userAgent.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(matchTest[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(userAgent) || [];
        return 'IE '+(tem[1] || '');
    }
    if(matchTest[1]=== 'Chrome'){
        tem= userAgent.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    matchTest= matchTest[2]? [matchTest[1], matchTest[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= userAgent.match(/version\/(\d+)/i))!= null) matchTest.splice(1, 1, tem[1]);
    return matchTest.join(' ');
}

  versionOS(){
    let OSName = '';
    if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
    if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
    if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
    if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";
    return OSName
  }
}
