import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'separadorMilesPipe'
})
export class SeparadorMilesPipe implements PipeTransform {
  // transform(value: number | string, locale?: string): string {
  //   return new Intl.NumberFormat(locale, {
  //     minimumFractionDigits: 0
  //   }).format(Number(value));
  // }
  transform(item: any, replace: string = ',', replacement = '.'): any {
    if (item == null) {
      return '';
    } else {
      for (let i = 0; i < 9; i++) {
        item = item.replace(replace, replacement);
      }
    }

    return item;
  }
}
