import { Pipe, PipeTransform } from '@angular/core';
import * as rutHelpers from 'rut-helpers';

@Pipe({
  name: 'rutFormat'
})
export class RutFormatPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    const formattedRut: string = rutHelpers.rutFormat(value) || '';
    return formattedRut;
  }
}
