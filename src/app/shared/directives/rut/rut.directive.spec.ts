import { Component, DebugElement } from '@angular/core';
import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from '@angular/core/testing';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  NgControl,
  ReactiveFormsModule
} from '@angular/forms';
import { By } from '@angular/platform-browser';

import { RutDirective } from './rut.directive';

@Component({
  template: `
    <form [formGroup]="form" novalidate>
      <input formControlname="rut" appFormatRut />
    </form>
  `
})
class TestRutComponent {
  form: FormGroup;
  constructor(private fb: FormBuilder) {
    this.form = fb.group({
      rut: [null, []]
    });
  }
}

xdescribe('Directive: RutDirective', () => {
  let component: TestRutComponent;
  let fixture: ComponentFixture<TestRutComponent>;
  let inputEl: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RutDirective, TestRutComponent],
      imports: [ReactiveFormsModule],
      providers: [NgControl]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestRutComponent);
    component = fixture.componentInstance;
    inputEl = fixture.debugElement.query(By.css('input'));
  });

  it('should create test component', () => {
    expect(component).toBeTruthy();
  });

  it('should format value', fakeAsync(() => {
    const event = {
      target: { value: '19' },
      preventDefault: function() {}
    };
    //console.log(inputEl.nativeElement);
    inputEl.triggerEventHandler('input', event);
    fixture.detectChanges();
    tick();
    expect(component.form.value['rut']).toBe('1-9');
  }));
});
