import { NgControl } from '@angular/forms';
import { Directive, EventEmitter, Output, HostListener } from '@angular/core';
import * as rutHelpers from 'rut-helpers';

@Directive({
  selector: '[formControlName][appFormatRut]'
})
export class RutDirective {
  @Output() public rutChange: EventEmitter<any>;

  constructor(private formControl: NgControl) {}

  formatRut(rut: string = '') {
    const formattedRut: string = rutHelpers.rutFormat(
      this.formControl.control.value
    );
    this.formControl.control.setValue(formattedRut);
  }

  @HostListener('focus', ['$event'])
  onFocus(value: string) {
    this.formatRut(this.formControl.control.value);
  }

  @HostListener('keyup', ['$event'])
  onKeyUp(value: string) {
    this.formatRut(this.formControl.control.value);
  }

  @HostListener('blur', ['$event'])
  onBlur(value: string) {
    this.formatRut(this.formControl.control.value);
  }

  @HostListener('change', ['$event'])
  onChange(value: string) {
    this.formatRut(this.formControl.control.value);
  }
}
