import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable, NgModule } from '@angular/core';
//import { TimeoutError } from 'rxjs/util/TimeoutError';

import { LoggerService } from './core';

@Injectable()
export class AppErrorHandler implements ErrorHandler {
  constructor(private loggerService: LoggerService) {}

  call(error: any, stackTrace: any = null, reason: any = null) {}

  public handleError(error: any) {
    if (error instanceof HttpErrorResponse) {
      const code = error.status;
      if (code === 0 || code === 400) {
        this.loggerService.error({
          title: 'Revisa tu conexión',
          text:
            'Tu internet no está funcionando correctamente y no podemos conectarnos. ' +
            'Revísalo e intenta nuevamente.'
        });
      } else if (code === 401) {
        this.loggerService.error({
          title: 'Tu sesión ha expirado',
          text: 'Por motivos de seguridad, inicia sesión nuevamente.'
        });
      } else if (code === 500 || code === 502 || code === 403) {
        this.loggerService.error({
          title: 'Algo ha pasado',
          text:
            'No podemos conectarnos en este momento con nuestros servicios. Intenta nuevamente más tarde.'
        });
      }
    } else {
      this.loggerService.error({
        title: 'Error',
        text: 'Ha ocurrido un error',
        debug: error
      });
    }
  }
}
