import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ErrorServicioComponent } from './error-servicio.component';

describe('ErrorServicioComponent', () => {
  let component: ErrorServicioComponent;
  let fixture: ComponentFixture<ErrorServicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ErrorServicioComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
