import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentPrincipalTestComponent } from './component-principal-test.component';

describe('ComponentPrincipalTestComponent', () => {
  let component: ComponentPrincipalTestComponent;
  let fixture: ComponentFixture<ComponentPrincipalTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentPrincipalTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentPrincipalTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
