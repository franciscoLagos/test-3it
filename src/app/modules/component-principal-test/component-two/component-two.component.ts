import { Component, OnInit } from '@angular/core';
import { EncuestaService } from '../../../services/encuesta/encuesta.service';

@Component({
  selector: 'app-component-two',
  templateUrl: './component-two.component.html',
  styleUrls: ['./component-two.component.scss']
})
export class ComponentTwoComponent implements OnInit {
  displayedColumns: string[] = ['correo', 'bebida'];
  
  dataSource;
  public listadoEncuesta;

  constructor(private surveySer: EncuestaService) { }

  ngOnInit() {
    this.getListadoEncuesta();
  }


  getListadoEncuesta(){
    this.surveySer.resultadosGenerales().subscribe((result) => {
      console.log('result::',result)
      this.dataSource = result;
      this.listadoEncuesta = result;
       
      
      
    }, (err) => {
      
    });

  }


}
