import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { encuestaInterface } from 'src/app/interfaces/encuesta.interface';
import swal from 'sweetalert2';
import { EncuestaService } from '../../../services/encuesta/encuesta.service';
import { of } from 'rxjs';


@Component({
  selector: 'app-component-one',
  templateUrl: './component-one.component.html',
  styleUrls: ['./component-one.component.scss']
})
export class ComponentOneComponent implements OnInit {

  form: FormGroup;
  public formIsValid: boolean;
  //public encuesta: encuestaInterface;
  public encuesta: encuestaInterface = {
    email: '',
    bebida: ''
  }
  public disable: boolean;

  constructor(
    private fb: FormBuilder,
    private surveySer: EncuestaService
    
  ) {
  }

  
  ngOnInit() {
  

    this.form = this.fb.group({
      email: ['', Validators.email],
      bebida: ['', Validators.required]
    });

    
  }

  formIsOk(){
    if(this.form.get('email').valid && this.form.get('bebida').valid ){
      this.disable=true;
    }
    else{
      this.disable=true;
      //this.enviarEncuesta(this.encuesta);

    }
  }
  
  onSubmit() {
    
    
    this.encuesta.email=this.form.get('email').value;
    
    this.encuesta.bebida = this.form.get('bebida').value;
    if(this.form.get('bebida').value===1){
      this.encuesta.bebida = 'Light';
    }
    else if(this.form.get('bebida').value===2){
      this.encuesta.bebida = 'Sin Azucar';
    }
    else if(this.form.get('bebida').value===3){
      this.encuesta.bebida = 'Normal';
    }
    else if(this.form.get('bebida').value===4){
      this.encuesta.bebida = 'No tomo';
    }
    
    this.enviarEncuesta(this.encuesta);

    
  }


  enviarEncuesta(encuesta: encuestaInterface){
    this.surveySer.insertEncuesta(encuesta).subscribe((result) => {
      
      swal.fire({
        title: 'Encuesta guardada',
        text: 'Se ha almacenado la encuesta con exito.',
        icon: 'success',
        confirmButtonColor: '#28892B',
        confirmButtonText: 'Cerrar'
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        /*
        if (result.isConfirmed) {
          this.router.navigate(['./']);
        } else if (result.isDenied) {
          this.router.navigate(['./']);
        }*/
      })
      
      
    }, (err) => {
      let texto= '';
      if(err.status===409){
        texto = 'Ya existe información relacionada con este correo.';
      }
      else if(err.status===500){
        texto = 'Error tipo: ' + err.status + ', Reintente más tarde';
      }
     
      swal.fire({
        title: 'Error al guardar la información',
        text: texto,
        icon: 'error',
        confirmButtonColor: '#28892B',
        confirmButtonText: 'Cerrar'
      });
    });

  }


  

}
