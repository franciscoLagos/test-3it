
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { encuestaInterface } from 'src/app/interfaces/encuesta.interface';

@Injectable({
  providedIn: 'root'
})
export class EncuestaService {

  
  constructor(private http: HttpClient) { }
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  apiURL= 'http://localhost:8080/api/';
  
  
  
  //servicio post para enviar info cliente al back
  insertEncuesta(encuesta: encuestaInterface){
    
    return this.http.post<any>(`${environment.apiConnect.enviarEncuesta}`, JSON.stringify(encuesta))
    .pipe(
      retry(1)
    
    )
      
  }

  //servicio get resueltad general
  resultadosGenerales(){
    
    return this.http.get(`${environment.apiConnect.resultadoGeneral}`)
    .pipe(
      retry(1)
    
    )
      
  }
}
