**Proyecto para la Prueba Tecnica De 3IT**

Lo primero que debes hacer es desplegar la api rest para esta prueba tecnica ubicada en la siguiente
url, con sus pasos respectivos

**https://bitbucket.org/franciscoLagos/test_back_3it/src/master/**


Luego de realizado lo anterior, se debe clonar el proyecto en tu directorio de preferencia con el siguiente comando

1. Dirijirse al interior de la carpeta donde se clono el proyecto
2. en la consola escribir npm install y dar enter
3. Luego escribir ng serve y dar enter
4. Abrir el navegador de preferencia y ingresar al localhost:4200

---

